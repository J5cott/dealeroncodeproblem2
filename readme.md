# Sales Taxes Solution
My implementation of a solution for the second problem in the coding test.

## Problem

There are a variety of items for sale at a store. When a customer purchases items, they receive a receipt. The
receipt lists all of the items purchased, the sales price of each item (with taxes included), the total sales taxes for
all items, and the total sales price.  

Basic sales tax applies to all items at a rate of 10% of the item’s list price, with the exception of books, food, and
medical products, which are exempt from basic sales tax. An import duty (import tax) applies to all imported
items at a rate of 5% of the shelf price, with no exceptions.  

Write an application that takes input for shopping baskets and returns receipts in the format shown below,
calculating all taxes and totals correctly. When calculating prices plus tax, round the total up to the nearest 5
cents. For example, if a taxable item costs $5.60, an exact 10% tax would be $0.56, and the final price after adding
the rounded tax of $0.60 should be $6.20.


INPUT 1  
1 Book at 12.49  
1 Book at 12.49  
1 Music CD at 14.99  
1 Chocolate bar at 0.85  

OUTPUT 1  
Book: 24.98 (2 @ 12.49)  
Music CD: 16.49  
Chocolate bar: 0.85  
Sales Taxes: 1.50  
Total: 42.32  

INPUT 2  
1 Imported box of chocolates at 10.00  
1 Imported bottle of perfume at 47.50  

OUTPUT 2  
Imported box of chocolates: 10.50  
Imported bottle of perfume: 54.65  
Sales Taxes: 7.65  
Total: 65.15  

INPUT 3  
1 Imported bottle of perfume at 27.99  
1 Bottle of perfume at 18.99  
1 Packet of headache pills at 9.75  
1 Imported box of chocolates at 11.25  
1 Imported box of chocolates at 11.25  

OUTPUT 3  
Imported bottle of perfume: 32.19  
Bottle of perfume: 20.89  
Packet of headache pills: 9.75  
Imported box of chocolates: 23.70 (2 @ 11.85)  
Sales Taxes: 7.30  
Total: 86.53  

## Assumptions

All books are called 'book'  
All food has 'chocolate' in the description  
All medical products have 'pills' in the description  
All imported items have 'import' in the description  

The input for the shopping cart can be taken from the shopping-cart.json file which is included in the build of the app.  This file has one 'items' property that is a list of strings that represent what is in the shopping cart.

## Design

Tax calculations are delegated to the SalesTaxService.  This service has the ability to check if a tax should be applied to an item and the ability to calculate the tax for that item.  This service takes a tax rate and an implementation of a IDecideWhatGetsTaxed service.

Besides the tax rate, the taxation calculation for all items works the same way.  But determining whether or not an item should be taxed depends on what we are looking for.  There is one strategy where only imported items should be taxed and another strategy where all items that are not books, food, and medical products are taxed.

To handle this variation of functionality we delegate that responsibility to services that implement the IDecideWhatGetsTaxed interface.  Depending on the tax we are trying to apply we can pass in a unique implementation of that interface.

The Program class handles loading the input from the sample json file, passing the items through the tax services, and then printing the results to the console.