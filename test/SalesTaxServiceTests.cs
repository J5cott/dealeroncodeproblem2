using App;
using Moq;
using Xunit;

namespace Test
{
    public class SalesTaxServiceTests
    {
        
        [Fact]
        public void TestPerfumeNeedsSalesTax(){
            var perfume = new Item(){
                Description = "Bottle of perfume",
                UnitPrice = 5
            };

            var service = new AllItemsTaxStrategy();

            var needsTax = service.ShouldApplyTax(perfume);

            Assert.True(needsTax);
        }

        [Fact]
        public void TestBooksChocolatesAndPillsDoNotNeedSalesTax(){
            var book = new Item(){
                Description = "Book",
                UnitPrice = 5
            };
            var chocolate = new Item(){
                Description = "Chocolate bar",
                UnitPrice = 5
            };
            var pills = new Item(){
                Description = "Packet of headache pills",
                UnitPrice = 5
            };

            var service = new AllItemsTaxStrategy();

            var bookActual = service.ShouldApplyTax(book);
            var chocolateActual = service.ShouldApplyTax(chocolate);
            var pillsActual = service.ShouldApplyTax(pills);

            Assert.False(bookActual);
            Assert.False(chocolateActual);
            Assert.False(pillsActual);
        }

        [Fact]
        public void TestOneDollarPerfumeGets10CentsOfTax(){
            var expected = 0.1m;
            var perfume = new Item(){
                Description = "Bottle of perfume",
                UnitPrice = 1
            };

            var service = new SalesTaxService(getCongressPersonMock().Object, 0.1m);

            var itemWithTax = service.ApplyTax(perfume);

            Assert.Equal(expected, itemWithTax.Tax);
        }

        [Fact]
        public void TestTenDollarPerfumeGetsOneDollarOfTax(){
            var expected = 1;
            var perfume = new Item(){
                Description = "Bottle of perfume",
                UnitPrice = 10
            };

            var service = new SalesTaxService(getCongressPersonMock().Object, 0.1m);

            var itemWithTax = service.ApplyTax(perfume);

            Assert.Equal(expected, itemWithTax.Tax);
        }

        [Fact]
        public void TestImportedBoxOfChocolatesShouldBeTaxed(){
            var chocolate = new Item(){
                Description = "Imported box of chocolates",
                UnitPrice = 5
            };

            var service = new ImportedItemsTaxStrategy();

            var actual = service.ShouldApplyTax(chocolate);

            Assert.True(actual);
        }

        [Fact]
        public void TestBoxOfChocolatesShouldNotBeTaxed(){
            var chocolate = new Item(){
                Description = "box of chocolates",
                UnitPrice = 5
            };

            var service = new ImportedItemsTaxStrategy();

            var actual = service.ShouldApplyTax(chocolate);

            Assert.False(actual);
        }

        [Fact]
        public void TestTenDollarImportedBoxOfChocolatesGetsFiftyCentTax(){

            var expected = 0.5m;

            var chocolate = new Item(){
                Description = "Imported box of chocolates",
                UnitPrice = 10
            };

            var service = new SalesTaxService(getCongressPersonMock().Object, 0.05m);

            var actual = service.ApplyTax(chocolate);

            Assert.Equal(expected, actual.Tax);
        }

        [Fact]
        public void TestFiftySixCentsRoundedUpToSixtyCents(){

            var expected = 0.6m;

            var chocolate = new Item(){
                Description = "Imported box of chocolates",
                UnitPrice = 5.6m
            };

            var service = new SalesTaxService(getCongressPersonMock().Object, 0.1m);

            var actual = service.ApplyTax(chocolate);

            Assert.Equal(expected, actual.Tax);
        }

        [Fact]
        public void TestFiftyFiveCentsStaysAtFiftyFiveCents(){

            var expected = 0.55m;

            var chocolate = new Item(){
                Description = "Imported box of chocolates",
                UnitPrice = 5.5m
            };

            var service = new SalesTaxService(getCongressPersonMock().Object, 0.1m);

            var actual = service.ApplyTax(chocolate);

            Assert.Equal(expected, actual.Tax);
        }

        [Fact]
        public void TestFiftyFourCentsRoundsUpToFiftyFiveCents(){

            var expected = 0.55m;

            var chocolate = new Item(){
                Description = "Imported box of chocolates",
                UnitPrice = 5.4m
            };

            var service = new SalesTaxService(getCongressPersonMock().Object, 0.1m);

            var actual = service.ApplyTax(chocolate);

            Assert.Equal(expected, actual.Tax);
        }

        private Mock<IDecideWhatGetsTaxed> getCongressPersonMock(){
            return new Mock<IDecideWhatGetsTaxed>();
        }
    }
}