using System;

namespace App
{
    public class SalesTaxService
    {
        private readonly IDecideWhatGetsTaxed _congressPerson;
        private readonly decimal _taxRate;

        public SalesTaxService(IDecideWhatGetsTaxed congressPerson, decimal taxRate){
            _congressPerson = congressPerson;
            _taxRate = taxRate;
        }
        
        public bool ShouldApplyTax(Item item){
            return _congressPerson.ShouldApplyTax(item);
        }

        public Item ApplyTax(Item item){
            item.Tax += item.UnitPrice * _taxRate;

            var roundedVal = Math.Round(item.Tax, 1, MidpointRounding.AwayFromZero);

            var roundingDiff = roundedVal - item.Tax;
            var positiveDiff = Math.Abs(roundingDiff);
            var roundTo = 0.05m;
            var shouldRound = positiveDiff < roundTo;
            
            if(shouldRound && roundingDiff > 0){
                item.Tax += roundingDiff;
            } else if(shouldRound && roundingDiff < 0){
                item.Tax += roundTo - positiveDiff;
            }


            return item;
        }
    }
}