using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace App
{
    public class ImportedItemsTaxStrategy : IDecideWhatGetsTaxed
    {
        private readonly Regex _importSniffer;

        public ImportedItemsTaxStrategy(){
            _importSniffer = new Regex(@"import");
        }
        public bool ShouldApplyTax(Item item)
        {
            return _importSniffer.IsMatch(item.Description.ToLower());
        }
    }
}