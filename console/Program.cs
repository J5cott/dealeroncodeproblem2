﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using Newtonsoft.Json;

namespace App
{
    class Program
    {
        private const string UnitPriceRegex = @"at [0-9,]+(\.[0-9]*)?";
        private const string QuantityRegex = @"^(\d{3},)+\d{3}|^\d+";
        private readonly Regex _quantity;
        private readonly Regex _unitPrice;
        private readonly SalesTaxService _allItemTaxService;
        private readonly SalesTaxService _importedItemTaxService;
        public Program(){
            _allItemTaxService = new SalesTaxService(new AllItemsTaxStrategy(), .1m);
            _importedItemTaxService = new SalesTaxService(new ImportedItemsTaxStrategy(), .05m);
            _quantity = new Regex(QuantityRegex);
            _unitPrice = new Regex(UnitPriceRegex);
        }

        public void Run()
        {
            var shoppingCart = JsonConvert.DeserializeObject<ShoppingCart>(File.ReadAllText("./shopping-cart.json"));

            var extractedItems = parseShoppingCart(shoppingCart);

            foreach(var item in extractedItems){
                AddTaxes(item, _allItemTaxService);
                AddTaxes(item, _importedItemTaxService);
            }

            printToConsole(extractedItems);
        }

        private void printToConsole(IEnumerable<Item> items){
            var groupedItems = items.GroupBy(x => new { x.Description, x.UnitPrice}).ToDictionary(x => x.Key, x => x.ToList());

            foreach(var item in groupedItems.Values){
                var firstItemInGroup = item[0];
                var baseString = $"{firstItemInGroup.Description}: {displayTwoDecimalPlaces(item.Select(x => x.TotalCostWithoutTax + x.TotalTax).Sum())}";
                var count = item.Count + item.Select(x => x.Quantity - 1).Sum();
                if(count > 1){
                    
                    baseString += $" ({count} @ {displayTwoDecimalPlaces(firstItemInGroup.UnitPrice + firstItemInGroup.Tax)})";
                }
                Console.WriteLine(baseString);
            }
            var totalTax = items.Select(x => x.TotalTax).Sum();
            Console.WriteLine($"Sales Taxes: {displayTwoDecimalPlaces(totalTax)}");
            Console.WriteLine($"Total: {displayTwoDecimalPlaces(totalTax + items.Select(x => x.TotalCostWithoutTax).Sum())}");
        }


        private string displayTwoDecimalPlaces(decimal value){
            return String.Format("{0:0.00}", value);
        }

        private void AddTaxes(Item item, SalesTaxService service){
            if(service.ShouldApplyTax(item)){
                service.ApplyTax(item);
            }
        }

        private IEnumerable<Item> parseShoppingCart(ShoppingCart cart){
            return cart.items.Select(x => {
                var result = new Item();
                result.UnitPrice = decimal.Parse(_unitPrice.Match(x).Value.Substring(2));
                result.Quantity = int.Parse(_quantity.Match(x).Value, NumberStyles.AllowThousands);
                x = Regex.Replace(x, QuantityRegex, "");
                x = Regex.Replace(x, UnitPriceRegex, "").Trim();
                result.Description = x;

                return result;
            }).ToList();
        }

        static void Main(string[] args)
        {
            new Program().Run();
        }
    }
}
