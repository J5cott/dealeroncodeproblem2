namespace App
{
    public class Item
    {
        public int Quantity {get; set;}
        public string Description {get; set;}
        public decimal UnitPrice {get; set;}
        public decimal Tax {get; set;}

        public decimal TotalTax
        {
            get
            {
                return (Quantity * Tax);
            }
        }

        public decimal TotalCostWithoutTax
        {
            get
            {
                return (Quantity * UnitPrice);
            }
        }
    }
}