using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace App
{
    public class AllItemsTaxStrategy : IDecideWhatGetsTaxed
    {

        private readonly IEnumerable<Regex> _watchList;

        public AllItemsTaxStrategy(){
            _watchList = new Regex[3] {
                new Regex(@"book"),
                new Regex(@"chocolate"),
                new Regex(@"pill")
            };
        }
        public bool ShouldApplyTax(Item item)
        {
            return !_watchList.Any(excludedItem => excludedItem.IsMatch(item.Description.ToLower()));
        }
    }
}