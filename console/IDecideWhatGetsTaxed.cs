namespace App
{
    public interface IDecideWhatGetsTaxed
    {
        bool ShouldApplyTax(Item item);
    }
}